Framework Architecture Details 

Folder Struture and details impelemtation 

1) Config File 
    --- Its contains the global data set file which contains
        the path of drivers , URL, browser Name for testing ,
         wait etc 

2) ExtentntsReport 
  ---- For report genration and test cases loger status and         view report in HTML all logic would be impelmented here


3)New Driver 
  ---- This folder contains all the driver exe file which need 
       to execute test cases on multiple browser

4) ScreenShot 
    ----- This folder will store all the screen shot during 
          test cases execution if test cases failed


SRC-->Test-->Java
1)CongigReader Class 
  ---- This class is responsible for read and write config file 
       and load confile file path etc
 
2)FileReader
  ---- This package class is responsible to read and write
       excell sheet which having Test data 

3)ObjectRepoReader 
  ---- This package class is responsible to read object
        repository where we put all the test pages locators

4)TestBases
  ---- Test Bases package having class which will contains all 
        logoc for iniate driver , open browser methods , close driver methods and some loader class object and all the reusable code which will need to extends each test class

5)Test Cases 
 ---- Here we put actual test scenario and all the assertion 

6) TestPages
 ---- Here all the test page and their respective action wouyld be written and we have to give construtor for initiate driver and this page object will use in test casess package class

7) Utills
 ---- Here all the utility class and impelmentation would be implemented like testlistener , extent report etc 

pom.xml file 

--- This is maven genrated file which contains all the depedency
    required for run the project 

TestNG.XML
--- This file is use for running the test in suite level here we pass the classes name and listerners class etc 

       

