package TestCases;

import static org.testng.Assert.assertEquals;

import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import FileReader.ExcelReader;
import TestBases.TestCasesBase;
import TestPages.SignInPage;
import Utills.ExtentManager;
import Utills.ExtentTestManager;

public class CreateAnAccount extends TestCasesBase {
	
	SignInPage signinpage ;
	
	@BeforeTest()
	public void openAppOnBrowser()
	{
		TestCasesBase.openBrowser();
		TestCasesBase.openApplication();
		
	}
	
	@Test(priority = 0, description = "Test with valid Email address")
	public void CreateAcccountWithValidEmail(Method method) throws Exception {
		ExtentTestManager.startTest(method.getName(), "valid email id");
		signinpage = new  SignInPage(driver);
		signinpage.clickOnSignInButton();
		signinpage.enterEmail(ExcelReader.getCellData(1, 2));
		signinpage.clickOnCreateAnAccountBtn();
		
		
	}
	
	@Test(priority = 1, description = "Test with valid Email address")
	public void create_Account_With_Invalid_EmailId(Method method) throws Exception {
		ExtentTestManager.startTest(method.getName(), "Invalid Email id");
		signinpage = new  SignInPage(driver);
		signinpage.clickOnSignInButton();
		signinpage.enterEmail(ExcelReader.getCellData(1, 3));
		signinpage.clickOnCreateAnAccountBtn();
		
		String actual = signinpage.getText();
		Assert.assertEquals(actual, "Invalid email address.");
		
	}
	
	@Test(priority = 2, description = "Test capture screen shot for failed test cases")
	public void test_capture_ScreenShot_For_Faild_TestCases(Method method) throws Exception {
		
		ExtentTestManager.startTest(method.getName(), "capture screeen shot for faild test cases");
		signinpage = new  SignInPage(driver);
		signinpage.clickOnSignInButton();
		signinpage.enterEmail(ExcelReader.getCellData(1, 3));
		signinpage.clickOnCreateAnAccountBtn();
		
		String actual = signinpage.getText();
		Assert.assertEquals(actual, "Invalid email addres.");
		
	}
	
	
	@AfterTest
	public void tearDown() {
		
		driver.close();
		driver.quit();
		
	}

}
