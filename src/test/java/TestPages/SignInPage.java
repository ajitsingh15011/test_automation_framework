package TestPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;

import ObjectRepoReader.ObjectMap;

public class SignInPage {
	
	public WebDriver driver;
	ObjectMap mapobj; 
	
	public SignInPage(WebDriver driver) {
		
		this.driver = driver;
	}
	
	
	public void clickOnSignInButton() {
		
		mapobj = new ObjectMap();
		WebElement  signInbtn = driver.findElement(mapobj.getObjectLocator("signInBtn"));
		signInbtn.click();
		
	}
	
	public void enterEmail(String email) {
		
		mapobj = new ObjectMap();
		WebElement emailtextbox = driver.findElement(mapobj.getObjectLocator("emailAddress"));
		emailtextbox.sendKeys(email);
	}
	
	public void clickOnCreateAnAccountBtn() throws InterruptedException {
		
		mapobj = new ObjectMap();
		WebElement createAnAccountBtn = driver.findElement(mapobj.getObjectLocator("createAnAccount"));
		createAnAccountBtn.click();
		Thread.sleep(5000);
	}
	
	public String getText() {
		
		WebElement  alertError = driver.findElement(mapobj.getObjectLocator("errroAlert"));
		String actualText = alertError.getText();
		return actualText;
	}

}
