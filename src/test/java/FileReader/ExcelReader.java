package FileReader;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader
{

	private static XSSFSheet sheet;

	private static XSSFWorkbook workbook;

	private static XSSFCell Cell;

	private static XSSFRow Row;
	
	
	//This method is to read the test data from the Excel cell, in this we are passing parameters as Row num and Col num

    public static String getCellData(int RowNum, int ColNum) throws Exception{
    	
			try{
				
				//String datasheetpath = System.getProperty("user.dir");
				
				File file = new File("./TestData/TestData_ZP.xlsx");
				FileInputStream fis = new FileInputStream(file);
				
				workbook = new XSSFWorkbook(fis);
				
				sheet = workbook.getSheet("testdata");

  			   Cell = sheet.getRow(RowNum).getCell(ColNum);

  			   String CellData = Cell.getStringCellValue();

  			   return CellData;

  			}
			catch (Exception e){

				return"";

  			}

    }

	
	
	
	
}
